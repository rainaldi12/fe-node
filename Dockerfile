FROM node:14.21.2 AS builder

WORKDIR /home/node/app

COPY . .

RUN npm install
# Building the production-ready application code - alias to 'nest build'
#RUN yarn install --production

#RUN yarn add reportWebVitals

RUN npm run build

FROM nginx:alpine

WORKDIR /var/www/react

# Copying the production-ready application code, so it's one of few required artifacts
COPY --from=builder --chown=node /home/node/app/build .
COPY fe-nginx/fe.conf /etc/nginx/conf.d

